if ($('.circle__ring').length) {
  var ctx = document.getElementById('circleRing').getContext('2d');
  var chart = new Chart(ctx, {
      // The type of chart we want to create
      type: 'doughnut',

      // The data for our dataset
      data: {
          datasets: [{
              backgroundColor: ['#f8961c', '#f2f2f2'],
              borderColor: 'rgba(255, 99, 132, 0)',
              data: [45, 55]
          }]
      },

      // Configuration options go here
      options: {
        aspectRatio: 1,
        cutoutPercentage: 78,
        events: []
      }
  });

  ctx.canvas.parentNode.style.height = '211px';
  ctx.canvas.parentNode.style.width = '211px';
}
