var itemPositioning = function () {

  var programTimeList = $('.program__time-wrapper');

  var timeNormal = function (i) {
    var decimalTimeString = i;
    var n = new Date(0, 0);
    n.setMinutes(+decimalTimeString * 60);
    var result = n.toTimeString().slice(0, 5);
    return result;
  }

  var programDayFilling = function () {
    var dayStarts = programTimeList.data('day-start');
    var dayEnds = programTimeList.data('day-end');
    for (var i = dayStarts; i <= dayEnds; i += 1) {
      programTimeList.append('<div class="program__time" data-time="' + i + '">' + timeNormal(i) + '</div>')
    }
  }

  programDayFilling();

  var itemSorting = function () {
    var programContent = $('.program__content');

    programContent.find('.item').sort(function (a, b) {
        return +a.dataset.start - +b.dataset.start;
      })
      .appendTo(programContent);
  }

  itemSorting();

  var itemTimeNormal = function () {
    var item = $('.item');
    item.each(function () {
      var itemTimeStart = $(this).data('start');
      var itemTimeEnd = $(this).data('end');
      $(this).find('.item__time').html(timeNormal(itemTimeStart) + ' - ' + timeNormal(itemTimeEnd));
    });
  }

  itemTimeNormal();

  var itemConflicts = function() {
    $('.item--conflicting').wrapAll('<div class="program__items-row"></div>');
    $('.program__items-row').each(function() {
      var conflictOffset = $(this).find('.item:first-child').position().top;
      $(this).css('top', conflictOffset);
    });
    setTimeout(function() {
      $('.program__items-row').find('.item').addClass('item--solved');
    }, 100);
  }

  var itemTimeRangeAll = [];
  var itemPositionTopList = [];

  var itemRangeFilling = function (item, timeStart, timeEnd) {

    var itemTimeRange = [];

    for (var i = timeStart; i < timeEnd; i += 1) {
      itemTimeRange.push(i);
    }

    itemTimeRangeAll.push(itemTimeRange);
  }

  var itemPositioningTop = function (item, timeStart, timeEnd) {
    var itemHeight = (timeEnd - timeStart) * 90;
    var itemStartWhole = Math.round(timeStart);
    var itemStartDecimal = timeStart - itemStartWhole;
    var timeSection = programTimeList.find(`[data-time='${itemStartWhole}']`);
    var timeSectionOffset = timeSection.height() * itemStartDecimal;
    var itemPositionTop = timeSection.position().top + timeSectionOffset + 8 + 'px';
    item.css('height', itemHeight + 8 + 'px');
    item.css('top', itemPositionTop);
  }

  $('.item').each(function () {
    var timeStart = $(this).data('start');
    var timeEnd = $(this).data('end');
    itemRangeFilling($(this), timeStart, timeEnd);
    itemPositioningTop($(this), timeStart, timeEnd);
//    $(this).wrap('<div class="program__item-wrapper"></div>');
//    itemPositioningLeft($(this), timeStart, timeEnd);
  });

  $('.program__content').css('height', Math.max.apply(Math, itemPositionTopList) + 150);

  itemConflicts();
};

itemPositioning();




