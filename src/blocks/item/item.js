//var itemPositioning = function () {
//
//  var programTimeList = $('.program__time-wrapper');
//
//  var timeNormal = function (i) {
//    var decimalTimeString = i;
//    var n = new Date(0, 0);
//    n.setMinutes(+decimalTimeString * 60);
//    var result = n.toTimeString().slice(0, 5);
//    return result;
//  }
//
//  var programDayFilling = function () {
//    var dayStarts = programTimeList.data('day-start');
//    var dayEnds = programTimeList.data('day-end');
//    for (var i = dayStarts; i <= dayEnds; i += 0.5) {
//      programTimeList.append('<div class="program__time" data-time="' + i + '">' + timeNormal(i) + '</div>')
//    }
//  }
//
//  programDayFilling();
//
//  var itemSorting = function () {
//    var programContent = $('.program__content');
//
//    programContent.find('.item').sort(function (a, b) {
//        return +a.dataset.start - +b.dataset.start;
//      })
//      .appendTo(programContent);
//
//  }
//
//  itemSorting();
//
//  var itemTimeNormal = function () {
//    var item = $('.item');
//    item.each(function () {
//      var itemTimeStart = $(this).data('start');
//      var itemTimeEnd = $(this).data('end');
//      $(this).find('.item__time').html(timeNormal(itemTimeStart) + ' - ' + timeNormal(itemTimeEnd));
//    });
//  }
//
//  itemTimeNormal();
//
//  var itemTimeRangeAll = [];
//  var itemPositionTopList = [];
//
//  var itemRangeFilling = function (item, timeStart, timeEnd) {
//
//    var itemTimeRange = [];
//
//    for (var i = timeStart; i < timeEnd; i += 0.5) {
//      itemTimeRange.push(i);
//    }
//
//    itemTimeRangeAll.push(itemTimeRange);
//  }
//
//  var itemPositioningLeft = function (item, timeStart, timeEnd) {
//    var itemWidth = (timeEnd - timeStart) * 200;
//    var itemPositionLeft = programTimeList.find(`[data-time='${timeStart}']`).position().left;
//    item.css('left', itemPositionLeft);
//    item.css('width', itemWidth + 'px');
//  }
//
//  var itemPositioningTop = function (item, timeStart) {
//    var lastConflictingItemIndex = 0;
//    var itemConflictingRows = [];
//
//    var rowNumber = 0;
//    for (var m = 0; m < itemTimeRangeAll.length - 1; m++) {
//      if (itemTimeRangeAll[m].indexOf(timeStart) >= 0) {
//        rowNumber += 1;
//        lastConflictingItemIndex = m;
//        itemConflictingRows.push($('.item').eq(lastConflictingItemIndex).data('row'));
//      }
//    }
//
//    if (lastConflictingItemIndex > 0) {
//      var lastConflictingRow = $('.item').eq(lastConflictingItemIndex).data('row');
//      rowNumber = lastConflictingRow + 1;
//    }
//    itemConflictingRows.sort(function (a, b) {
//      return a - b;
//    });
//
//    for (var p = 0; p < itemConflictingRows.length; p++) {
//      if (itemConflictingRows[p] != p) {
//        rowNumber = p;
//      } else {
//        rowNumber = p + 1;
//      }
//    }
//
//    var itemPositionTop = rowNumber * 85;
//    itemPositionTopList.push(itemPositionTop);
//
//
//    item.css('top', itemPositionTop + 'px');
//    item.attr('data-row', rowNumber);
//  };
//
//  $('.item').each(function () {
//    var timeStart = $(this).data('start');
//    var timeEnd = $(this).data('end');
//    itemRangeFilling($(this), timeStart, timeEnd);
//    itemPositioningLeft($(this), timeStart, timeEnd);
//    itemPositioningTop($(this), timeStart);
//  });
//
//  $('.program__content').css('height', Math.max.apply(Math, itemPositionTopList) + 150);
//
//};
//
//itemPositioning();
